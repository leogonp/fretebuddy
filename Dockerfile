# Define a imagem base
FROM node:14

# Define o diretório de trabalho dentro do container
WORKDIR /app

# Instala as dependências do projeto
RUN npm install

RUN npm init -y
RUN npm install express axios body-parser dotenv multer form-data

# Copia todo o conteúdo do diretório atual (onde está o Dockerfile) para o diretório de trabalho do container (/app)
COPY . .

# Expõe a porta 3000 para que possamos acessar o servidor Node.js fora do container
EXPOSE 3000

# Comando para iniciar o servidor Node.js
CMD ["node", "app.js"]