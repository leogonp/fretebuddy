const express = require('express');
const axios = require('axios');
const bodyParser = require('body-parser');
const multer = require('multer');
const FormData = require('form-data'); 
const upload = multer();

require('dotenv').config();

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.post('/api/audio/transcribe', upload.single('file'), async (req, res) => {
    try {
        const audioData = req.file.buffer;
  
        const formData = new FormData();
        formData.append('file', audioData, {
          filename: 'audio.mp4', 
          contentType: 'audio/mp4', 
        });
        formData.append('model', 'whisper-1');
  
        const openaiResponseTranscription = await axios.post(
          'https://api.openai.com/v1/audio/transcriptions',
          formData,
          {
            headers: {
              'Authorization': `Bearer ${process.env.OPENAI_KEY}`,
              ...formData.getHeaders(),
            },
          }
        ).catch(error => {
          console.error('Erro ao transcrever o áudio:', error.message);
          res.status(500).json({ error: 'Erro ao transcrever o áudio.' });
          return;
        });
  
        const transcription = openaiResponseTranscription.data;
        
        const openaiResponse = await axios.post(
          'https://api.openai.com/v1/completions',
          {
              model: "davinci:ft-fretecom-2023-07-30-08-28-22",
              prompt: "O usuário envia: " + transcription.text.trim() + "Como o assistente GPT deve responder?",
              max_tokens: 100,
              temperature: 0.1,
              top_p: 1,
              frequency_penalty: 0.5,
              presence_penalty: 0.8,
              best_of: 1,
              n: 1,
              stop: ["\n", "O usuário responde", "O usuário"]
          },
          {
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${process.env.OPENAI_KEY}`,
            },
          }
        ).catch(error => {
          console.error('Erro ao buscar resposta do modelo:', error.message);
          res.status(500).json({ error: 'Erro ao buscar resposta do modelo.' });
          return;
        });
  
        let botReply = openaiResponse.data.choices[0].text.trim();

        const regex = /\[redirect:\s*(.*?)\]/;
        const match = botReply.match(regex);

        const command = (match && match.length >= 2) ? match[1] : null;

        botReply = botReply.replace(/\[.*?\]/g, '');

        const queryParams = new URLSearchParams(
          {
              key: process.env.VOICERSS_KEY,
              hl: 'pt-br',
              v: 'Ligia',
              src: botReply,
              r: 0,
              c: 'mp3',
              f: '44khz_16bit_stereo',
              ssml: false,
              b64: false,
          },
        ).toString();

        res.json({file: 'http://api.voicerss.org?' + queryParams, text: botReply, command: command});

    } catch (error) {
      console.error('Erro inesperado:', error.message);
      res.status(500).json({ error: 'Erro inesperado ao processar a requisição.' });
    }
});

app.listen(port, () => {
  console.log(`Servidor rodando em http://localhost:${port}`);
});