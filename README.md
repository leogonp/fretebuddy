# Fretebuddy

Esta aplicação foi desenvolvida utilizando Node.js e faz uso do framework Express para criação de servidores web. Através dela, é possível interpretar um áudio inputado pelo caminhoneiro para interpretarmos a sua dúvida e sugerir a melhor resposta possível utilizando uma IA.


## Instalação

Para instalar e configurar a aplicação, siga digitar o seguinte comando:

```bash
make docker-up
```

## Configuração

Antes de executar a aplicação, é necessário configurar as chaves de API necessárias para os serviços de transcrição de áudio para texto e síntese de voz. Certifique-se de possuir as chaves de API dos seguintes serviços:

- OpenAI: A aplicação utiliza a API da OpenAI para realizar a transcrição de áudio para texto. Acesse https://platform.openai.com/ e siga as instruções para obter sua chave de API.

- VoiceRSS: O serviço VoiceRSS é utilizado para transformar texto em voz. Acesse https://www.voicerss.org/ e obtenha sua chave de API.

Para configurar essas chaves e também setar algumas outras configurações do aplicativo, crie um arquivo .env na raíz do projeto, contendo as seguintes variáveis:

```
PORT=3000
OPENAI_KEY=OPEN-API-KEY
VOICERSS_KEY=VOICERSS-KEY
```


### Rotas da API

A aplicação expõe as seguintes rotas de API:

1. `POST /api/audio/transcribe`: Esta rota permite enviar um arquivo de áudio para transcrição em texto. O arquivo de áudio deve ser enviado como um formulário com o campo `audio` contendo o arquivo. A resposta conterá o link do áudio da resposta.

Exemplo de requisição usando o comando `curl`:

```bash
curl --location 'localhost:3000/api/audio/transcribe' \
--header 'Authorization: Bearer OPENAPI-KEY' \
--form 'file=@"path/to/audio.oga"' \
--form 'model="whisper-1"'
```

Resposta:
```
{
    "file": "LINK-TO-AUDIO",
    "text": "TRANSCRIPTION-OF-THE-AUDIO",
    "command": "COMMAND-BASED-IN-THE-QUESTION"
}
```


## Considerações Finais

Certifique-se de que todas as dependências estejam instaladas e que as chaves de API estejam configuradas corretamente antes de iniciar a aplicação.

Em caso de dúvidas ou problemas, sinta-se à vontade para entrar em contato com a equipe de desenvolvimento.

Agradecemos por utilizar nossa aplicação!

---
